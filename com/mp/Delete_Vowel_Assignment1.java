package com.mp;

public class Delete_Vowel_Assignment1 {
    public static void main(String[] args)
    {
        String str = "E Hello";
        System.out.println("Given string is: " + str);
        str = str.replaceAll("[AaEeIiOoUu]", "");
        System.out.println("The string without vowels is: " + str);
    }
}
